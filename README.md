# 2D Flipscreen platform game template with scrolling
Current Version 1.3.0 (latest addition: scrolling rooms)

This project template lets you create 2D Tilemap based games with flipscreen room and scrolling navigation, with or without a dedicated HUD. Think of games like Links Awakening, Binding of Isaac, older games such as Jetset Willy, Dizzy, combination flip/scrolling games like Celeste or the original Metroid that we'll be creating below.

The project is a ready made template for a new game, but instructions are provided for integrating the components into your existing game.

![image-pt4-3.png](README_Images/image-pt4-3.png)

### Overview
The video tutorial for recreating one of the Metroid levels can be found here, and the full playlist also contains detailed videos on the creation of the library should you be so inclined:

https://www.youtube.com/watch?v=Cnx3ts86qDg&list=PLtc9v8wsy_BY8l7ViNJamL6ao1SKABM5T&index=6

If you want the full tutorial that teaches Godot while creating the flipscreen template follow this playlist:
https://gitlab.com/chucklepie-productions/tutorials/flipscreen-camera/


### Getting Started
Download the project template by selecting the download icon above and downloading the zip, then uncompress and open within Godot. If you're feeling adventurous then use the Clone option and download everything via git. 

You can also install it via the Godot Asset Library and directly from within Godot (start Godot, select 'Templates' tab and search for 'flipscreen'). Usually where you are now will have the most up to date version, so prefer here. But if you use Godot for ease then just check the version matches the version stated above.

![image.png](README_Images/image.png)

This project was saved with Godot 3.3. but should work with any 3.x version.

After downloading, open the project, run and ensure everything is working. You use cursors to move the ship.

### TL;DR
Assumes you know Godot and have watched the tuturial here are the brief, unexplained steps. Otherwise skip this section and continue to the template tutorial/guide.

A map is one or more Tilemaps with a fixed width and height for each room. Each map is given a number, by default it presumes 0 is top left going across and down. Using 'snapping options' toolbar and View/Always show grid to set a grid to view each room in the editor more easily. You do not need numbers if you are directly linking each room, but numbers are best as you can let the engine handle movement with maths :)

Without HUD:
1. Go to Project/Project Settings, find Display/Window and enter the size of the screen in Width/Height. This should match the size of each room, then in Application/Run property select scenes.game.tscn if not already set
2. Open scenes/map.tscn, delete the Map01 node then instance your own node to take its place, which must be a Node2D with at least one Tilemap as a child. Refer to the existing map.tscn
3. Select Map node and specify the number of screens across (10,20,30,etc make it easy to know room numbers) in 'Map Width Screens', then enter the 'Start Room' number. Save map.tscn
4. In map create your ROOM_DATA dictionary and add exits. Add any transitions and exit positions if required.
5. If adding scrolling, update the ScrollingRegions
6. Open camera.tscn and set 'Room Navigation Pixel Offset' to how far out of the screen the player must travel before changing rooms
7. If the node that triggers the room change (the player usually) does not use bit 0 for it's Collision Layer then edit camera.tscn and update the Collision Mask appropriatley for RoomBlocker and RoomNavigation
8. Open game.tscn scene, this is the 'game'. Select 'Map' and if Map Width Screens and Start Room are different click the revert icon to reset to the ones saved in steps above
9. Select Player node, move it in to position in your start room
10. Run the game and WASD should work for navigation and cursors for the player ship

To add a HUD at the top of the screen, 
1. Follow steps above for your game, ensuring at step 1 you enter the size of the screen, i.e. the game plus the size of the HUD
2. Open game_with_hud.tscn and (normally) enable 'Use Override Settings', set 'Viewport Size Override' to the size you want each room, i.e. just the room size, then in 'Viewport Start Location' enter the position to place the room (typically offset from where you place the UI)
    - Example: game is 320x200 and HUD is 320x50 and we want the HUD on top. The display settings in project settings should be 320x250 and the viewport size override should be 320x200, and the start location 0,50

Cleanup:
1. Delete game_with_hud scene/script files if not using hud
2. Delete game_background scene/script files if not using background layer in game
3. Delete game scene/script files if integrating into your game
4. Delete README file and folder (used by the git website)

### Detailed Instructions
#### Setup
The supplied test game is called Cybernoid and was written in 1988 by Raffaele Cecco. As stated in the licence file, all content comes under a permissive MIT licence, however the assets folder contains copyrighted material and cannot be used for commercial purposes. Please test this works fine before proceeding.

For this getting started guide, we will create our own version of the original Metroid game with the map Ridley's Hideout. Alternatively you can use your own or new Tilemap.

You can find the original map here:
https://nesmaps.com/maps/Metroid/Ridley.html

This game was a scrolling NES game, but we'll make it flip screen. Cybernoid is 512x320 and when the HUD is used this is 512x384. Metroid is 512x480 and when we enable the HUD later it will be 512x544. Remember these numbers :)

1. Open the file levels/metroid.tscn
2. On the toolbar click the three dots, click configure snap and set grid step to 512, 480

![image-3.png](README_Images/image-3.png)

3. Then click View toolbar button then click Always show grid. You should see this:

![tut-01.png](README_Images/tut_01.png)

4. This will show is every room in the map if you zoom out. The number of rooms across is 17 - the template system needs to know this. However, if we round it to 20 it'll make it easier to visual each room number, i.e. each new row will begin 0,20,40, etc, as shown below. Non-linear movement is detailed later.
![tut-09.png](README_Images/tut_09.png)


#### Adding a map
Referring to the above map room number image above might help when discussing map movement and data.

1. Open scenes/map.tscn

Map.tscn is a Node2D with a script that manages the nodes and extends a base Node2D called Map Master. The node Map01 is the actual tilemap and ScrollingRegions is a container for scrollings sections. If scrolling is not required you can delete ScrollingRegions. Map01 can be called anything but must be a Node2D and must contain at least one TileMap. Open scene see two tilemaps for two layers and scrolling area containing 1 area (you can have as many as you want).

Normally you will duplicate this file for each map/level, but to make things simple, we will simply reuse this file for Metroid. 

2. Delete Map01 scene instance and add our levels/metroid.tscn using the 'instance a scene button', i.e. we are keeping the Map controller but replacing our tilemap. The name of the new node is irrelevant.f

You can open this node, and you will see a Node and a Tilemap. There must always be a top-level node. You can put whatever you need inside this (e.g. more tilemaps for layers, enemy nodes, animations, etc.). The map searches for the first node to get basic information such as tile size.

![tut-02.png](README_Images/tut_02.png)

3. With Map node selected set Map Width Screen to 20 and set Start room to 0 in the Inspector tab and save

![tut-10.png](README_Images/tut_12.png)

4. Open game.tscn, select Map and ensure the details show as step 3. If not click the reset icon to the side of the two entries

5. Go to Project menu and Project Settings and find Application\Run and change it to game.tscn

7. Then, in settings, go to Display\Window and set width/height to be 512x480 and 1024x960 for the test width/height.

Now run the game and it should display ok as below. Stop the game. Remember, there should be no HUD.

![tut-10.png](README_Images/tut_10.png)

#### Map data
Open map.tscn and go to the code. This file contains the variable _ROOM_DATA (currently it is the data for Cybernoid). It stores a reference to each room and the exits that can be reached, i.e. it is how you navigate the map. Our tilemap only really supports movement to adjacent screens, but at the end we'll see how to fix this. For now we'll update the room data for the first few screens. The 'exits' array states the rooms available when travelling up,down,left,right. No navigation allowed is -1.

Delete the existing _ROOM_DATA and replace with this. Note -1 means no exit allowed.

```
const _ROOM_DATA = {
	#                  u,  d, l, r
	"-1" : {"exits": [-1,-1,-1,-1]	},
	"11" : {"exits": [-1,31,-1,-1]	},
	"31" : {"exits": [11,51,-1,-1]	},
	"51" : {"exits": [31,-1,50,52]	},
	"50" : {"exits": [-1,-1,-1,51]	},
	"52" : {"exits": [-1,-1,51,-1]	}
}
```

You should be able to see what we've done using the grid map image above. Perhaps in a later version this data will be moved to the map file. If you have many maps/levels you will have to duplciate all these files, maybe in a future version we'll allow just one map.tscn with multiple map children...

At the map 2D view, update Map node, set Start Room property to 11 and save (remember to check it is also updated in the game.tscn scene), this is our real start room.

Run the game, ensure you can see room 11 and you can now use WASD to navigate the first 6 screens (press S to move down as your only first move. In Metroid this is the shaft that takes you into the level.

#### Player
The supplied scenes/player.tscn is just for your testing and should obviously be replaced by your own eventually.

Let's put our existing spaceship in the game.
1. Open up scenes/game.tscn
2. Click Player node in the scene tree and set Z index to 1 so we can see it (our basic map includes black solid tiles)
3. Find the ship in the editor (hint: press F to centre on it)
3. Select the Move Mode Icon (W key) and drag the player to the start room 11, ensure the child node is moved too, we'll look at this later.

![tut-03.png](README_Images/tut_03.png)

We want our player to go 16 pixels out of the screen before a room change.
1. Open camera.tscn and set the pixel offset property in the inspector to 16, i.e. select Camera2DFlip and go to instance
2. Open up game.tscn, select Camera2DFlip and ensure it matches. If not, click the revert icon next to the property

![tut-03.png](README_Images/tut_11.png)

We open the instance so that this will be the default if we ever duplicate it, otherwise setting inside game only affects this instance.

Run the game, the ship will not collide with anything but room navigation will work. 

Stop the game, select the Player node. If you look at the Collision Layer is set to the first bit and the Mask to the second (hovering will show their names), i.e. Layer=Player and Mask=What player collides with.

### Map collisions
We need some basic collisions set up in our test Metroid map for movement and navigation to work.

Open up the tilemap file scenes/map.tscn and then open up your map scene and select TileMap so we are in the tilemap editor. The metroid already has collision polygons created (you can look by selecting the tilemap, selecting TileSet and looking at the tiles in the editor).

Find the Collision section and set Layer to be bit 1 (environment) and Mask to be bit 0 (Player), i.e. Layer=Tilemap/Environment and Mask=Tilemap Colliding with Player

Run the game and everything should now function. You are free to change collisions to whatever you want of course.

### Game testing and transitions
Open Room Manager instance, defaults state what the defaults are, try them all out. Transition time is ignore for 'None'. Remember at game instance check if they've copied over.

Reset back to None. Open map.tscn code and in Map ROOM_DATA update the array for transitions by remove 11 and 31 and replacing with this.

```
	"11" : {"exits": [-1,31,-1,-1],
		    "transitions": 	 [ [],[1,2.0],[],[] ]
			},
	"31" : {"exits": [11,51,-1,-1],
		    "transitions": 	 [ [],[2,1.0],[],[] ]
			},
```

Each entry is always an array for up, down, left right.

This says for all room 11 use type 1 transition when going down and take 2 seconds and in room 31 type 2 for 1 second going down, all other rooms are irrelevant as we have no exits. When not specified it uses the default we set and -1 means the same thing if put inside the array.

The transition type matches enum ROOM_TRANSITION stored in Room Manager but are 0,1,2 for none, slide, fade.

Note, going back into 11 or 31 will use the default not what we specified for going down.

Run and this should do this.

Edit the file scenes/game.gd, this is how it is done, via the manage direction code, you can force in code as shown.

Go to room manager code and observe the ROOM_TRANSITION enum, this is 0,1,2. You can add to this, well see later.

Note, the room manager in the Transition code will pause the entire game while it transitions. Every node by default will pause during this time. However the Room Manager and Camera nodes have pause mode set to 'process' so they will still work. If you do not want pausing or wish to alter this, you need to update these two nodes.

### Warping
Look at the map (or the image above), well fast travel the player from room 51 when exiting right across to 55. Edit ROOM_DATA in map.tscn for this entry:

```
	"51" : {"exits": [31,-1,50,55],
			"exit_positions":[ 	[],[],[],[200,250] ] 
			},
	"55": {"exits": [-1,-1,-1,-1]	},
```

We have changed 51 to go to 55 and added 55, otherwise the room manager will not allow travel. When we travel to the next room, the camera moves but the player remains at same position. So how do we warp the map and move the player?

This is what exit_positions does. You can add it to any room and it says for this direction when exiting the room place the player at this position relative to the top left of the room. You do not have to warp to do this.

Run the game, go to 51 then right, and you will warp directly in.

Now change to this and we have a nice slide warp
```
	"51" : {"exits": [31,-1,50,55],
			"transitions": 	 [ [],[],[],[1,2.0] ],
			"exit_positions":[ 	[],[],[],[200,250] ] 
			},
```

But the game has no reference to the player, so how does it work?

1. Go the game scene and select Room Manager. In the Inspector tab, select Signals instead.
2. Room Manager raises a signal telling anyone listening that the player should be repositioned
3. Double click and you'll see in the game.gd we handle this by simply moving the player

You will need to do this in your game.

### Scrolling
Open game.tscn, notice there are two Cameras, Flip and Scroller. The room manager uses the flip camera by default but if it detects scrolling it switches to the scrolling camera.

Open Map.tscn, select ScrollingRegions and set z index to 1 (same as player, we just need to show it above the map). This checks for Player in collision like the map and has a single collision shape. When the player enters or exits this area is switches between the two cameras.

There may be issues with the camera jumping, best way is to resize the area. A lot of issues is when the edges are too close to room exits and the room manager exit room event triggers as well as camera change. Others are just an artefact of how it is and you can fix the code. 

By default it will not do anything when exiting scrolling sections but with force a room change when exiting a scrolling area. 

In Metroid Samus slides down from 11 to 51, so let's do that. Take the existing CollisionShape2D_1, and set these properties:

Transform/Position: 5890,590
CollisionShape2D/Shape/RectangleShape2D/Extents: 240,256

Run the game, it should start scrolling but jump on exit. This is the effect we mentioned. In this case it is because the scrolling camera will always start at the top of the current screen, if you can fix this then do so, otherwise, change values to this:

Transform/Position: 5890,440
CollisionShape2D/Shape/RectangleShape2D/Extents: 240,400

There are bugs in the scrolling, but these will eventually be fixed by me or you :)

Open game, select Camera2DScroller. The room manager sets the scrolling via the drag margin enabled flags and Limit section, but you are free to update anything you want, e.g. drag margin to allow more or less movement by the player or smoothing, etc. These values may affect how camera transitions work causing issues such as jerky camera movement. One fix, and is how Metroid does it, is when changing from scrolling to flip screen it performs a Slide transition to ensure the room is fully on the screen. You'll see this if you watch a video as player enters room 50 (the white tiled rooms).

The more observant will be wondering how is it scrolling with the player when the system has no reference to the player and the scrolling camera is not a child of Player (you need the camera to be a child to scroll the map).

1. Open the main game scene
2. Player has a RemoteTransform2D node, select this
3. Its path is set as Camera2DScroller

Remote Transform essentially lets you make the game think a node is somewhere else. In this case the Camera2DScroller's transform is now relative to the player. You obviously need to do this for your game.

## Non-linear rooms (or adjusting player on room change)
The flipscreen template presumes all rooms are in a grid with sequential numbers.

If you specify non-adjacent room navigation, e.g. instead of going right from room 50 to 51 you wanted to go to room 55 then the map will update correctly to room 55, including any transition effect you have. 

However, in either case the room manager only updates the camera, it does not know or care about the player. Confucious says the camera moves but the player remains the same.

To fix this, there is a ROOM_DATA entry called 'exit_positions'. Edit the map.tscn code, find ROOM_DATA and update the following:


```
	"51" : {"exits": [31,-1,50,55],
			"transitions": 	 [ [],[],[],[1,2.0] ],
			"exit_positions":[ 	[],[],[],[200,250] ] 
			},
	"55": {"exits": [-1,-1,-1,56]	},
	"56": {"exits": [-1,-1,-1,-1]	},
```

"55" is a new entry because we cannot move to a room that does not exist. "56" is to the right of this.

exit_positions is an array with 4 items, each item is an x/y co-ordinate array and states where to position the player in pixel co-ordinates local to the room (i.e. 0,0 is top left). You can simply set a new position when going to adjacent rooms. Here we say when exiting 51 right move to 55 using a slide transition and place the player at location 200,250 (0,0 is top left of room 55).

Because the room manager has no knowledge of the player it simply raises a signal 'map_reposition_player'. You will need to handle this, typically in your game. So open game.tscn, select RoomManager, go to the Node tab (next to Inspector) and this is already coded, but double click and all it does is this:

```
func _on_RoomManager_map_reposition_player(new_global_position) -> void:
	$Player.global_position=new_global_position
```

While we're here, lets do some scrolling. Notice on the map there is a large vertical set of screen running almost the entire length of the map, this is room 56.

1. Open Map.tscn
2. Select CollisionShape2D_1 and duplicate (CTRL-D)
3. In the Inspector select the dropdown next to RectangleShape2D and 'Make Unique', this is so we can resize it
4. Set the details as follows:

Transform/Position: 8448,2400
CollisionShape2D/Shape/RectangleShape2D/Extents: 240,1440

Run game, go to room 51, go right and you should warp to 55. Then go right and you should then enter scrolling for the entire vertical section of the game.

### HUD
Metroid doesn't have a hud, but lets add one :)

We will simply add the HUD used for the Cybernoid default game. This HUD is 512x64 pixels. So we need to increase the screen height by 64 pixels. We'll briefly describe the HUD soon.

1. In Project menu and Project Settings go to the Display/Window and update height by 64 pixels, i.e. 544. Set the test width/height to 0, otherwise we will have a large window that may not fit your screen properly, but you can double these if you want
2. Set the Project Settings/Application/Run to be game_with_hud.tscn

Note: the default scaling for the template is 'Viewport' with 'Keep'. This is the preferred choice for 2D pixel games.

3. Open up the scenes/game_with_hud.tscn

Notice this simply includes a reference to scenes/game.tscn. We can therefore use either game.tscn directly or game_with_hud.tscn and the game will not change. If you do not use a HUD in your game, you can delete this scene file.

We will not be detailing the HUD, but as a brief introduction, first open the scene UI, this is a Canvas Layer (it will always be on screen and is not affected by the camera). This is simply a set of gui controls to contain our HUD. For simplicitity it is a texture rectangle holding an image of our HUD. In your game this will also include other elements such as labels, grid containers, etc.

Note this is detailed fully in the tutorial series, part 4
https://gitlab.com/chucklepie-productions/tutorials/flipscreen-camera/flipscreen-part04/


Close and go back to game_with_hud.tscn.

4. Select GameWithHUD node

![tut-03.png](README_Images/tut_13.png)

5. Tick 'Use override settings'
6. Set viewport size override to be 512,480
7. Set viewport start location to be 0,64


Explanation: All games have a default viewport that covers the screen and is created by Godot - ours as just set in Project Settings is 512x544. In game_with_hud.tscn we have our UI Canvas Layer (our HUD) at the top and is drawn in the main viewport. We have two nodes (Viewport/Viewport Container) that creates a sub-viewport inside the main viewport. 

Specifying the values in these overrides tells the setup code to resize the Viewport and ViewportContainer nodes to be 512x480 (i.e. our room size) and to start at 0,64, just below the UI.

As mentioned, this HUD is just a placeholder, you need to add your own gui components obviously.

If you leave these values as -1 then the startup code does not resize the sub-viewport and assumes you have done it, i.e. you may want to have a layout unsupported by the template, or simply want to see the actual size and position at design time. This is detailed later.

In the game with HUD node select the Game instance (not the actual game.tscn file) and set 'Configure On Ready' to false. This is just a minor optimisation to stop the game scene configuring itself twice (with HUD and then again in game).

Run the game, everything should be just lovely.

### Coding your game

This is the end of the tutorial, what follows is guidance on what steps to take next and some section on manually updating the template.

Open up game.gd and go to the code. This contains the necessary code for using the template system, either use this as your starting point or integrate it into your game.

1. The onready variables hold references to the template nodes for camera, map and room manager
2. Ready function gets the viewport size and calls a method configure_game that will setup the template system
3. the room manager method setup_rooms is the entry point, you call this passing in references to the camera and map and the viewport size. This sets the camera correctly for standard room management (see later for adjusting this)

We call change_room so that on startup the start room is placed (-1 states to use whatever is the current room - on startup this is the start room property we set as 11).

The camera room change is a signal from the camera, we've already seen this. Here we tell the room manager to change room to the direction specified.

### Adding functionality to the Room Manager
Currently _ROOM_DATA dictionary only contains the 'exits' array, transitions and exit_positions. You may, and should, extend this to include other room data, e.g. number of enemies to spawn, etc. You should also either add some code to handle the change room signal raised by the template, or update the room template to create other signals, e.g.

```
"52": {
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"exits"			: [0,0,0,37]
	}
```

## Integrating with an existing game
If you are creating a game without a HUD then 

- put the RoomManager, Camera and Map scenes as top level nodes in your project.
- If you have an existing Camera it's probably best to use the flipscreen camera and move your changes in.
- If you have an existing Map, this is covered in the topics above, i.e. how we changed the template to use Metroid
- Use the code in game.gd in your game, i.e. ensure setup_rooms is called with the correct data so that the automatic camera setup works and you connect/reconnect the reposition player signal
- update the settings for the camera and map nodes

If you require a HUD then you need to create the node structure as detailed above.

#### Manual updating
It may be necessary to manually set things up, either due to how the map is configured or you want to see exactly how it looks in the editor. It is probably best to leave things for the automatic setup.

If you are using a HUD and wish to set the positions manually then follow the steps above by updating the Viewport/Viewport Container (and/or UI) then untick the 'use override settings' and ensure the override values are all -1

The camera.tscn contains RoomBlocker and RoomNavigation nodes. RoomBlocker is a static body with four polygons that block or allow the player node to leave the screen. By default it will take the viewport size (i.e. the room size) and put the blockers exactly around the edge. RoomNavigation is offset from RoomBlocker (therefore only reached when room blocker is disabled) and when reached raises a signal to the rest of the game to indicate a room change.

These currently only work for flipscreen, to block scrolling areas you need to do it yourself, e.g. using tiles or static bodies you update in code.

![tut-04.png](README_Images/tut_04.png)

If this is not what is required, you can manually set these.

However, if you have modified the camera manually you need to ensure it does not get automatically configured because the default mode is room manager will adjust the camera.

1. Open game.gd
2. Comment out the configure_game() call in _ready
3. In the section 'Coding your game' above, ensure the viewport passed in to the setup_rooms method is null

If you are modifying the camera you should also
- make a call to manual_config method on your map. Look at configure_game in game.gd for where to call this
- If you are using a HUD and modify the camera manually, it's probably best to manually update the viewport/viewport container as detailed above

As an alternative to automatic camera setup and manual update, you can call set_navigation_system on your camera, as also shown as an example in game.gd. This is actually what the template calls when you specify a non-null value during setup_rooms()

### How it all works
The game.tscn and game_with_hud.tscn are starter nodes for your game. If these are not being used then you need to copy their functionality into your game and ensure you have references to the room manager, map and camera nodes.

#### Flipscreen Camera
This contains a static body called RoomBlocker that is set to block the player exiting, and an Area2D node called RoomNavigation that triggers a room change when the player collides. These both use Collision Mask value to detect the body (player). These only work in flipscreen mode.

Methods:
- lock_exits:   blocks all exits. This might be handy for such things as room bosses, i.e. override default room exits
- set_navigation_system: This sets the position and size of the camera RoomBlocker and RoomNavigation. If allowing automatic configuration then this does not need to be called, it is called by the room manager during setup. If requires the game room size in pixels and the offset from the edge for the player node when exiting
- set_position: this should only be called by the room manager, it changes the camera position to a new room
- set_exits:    blocks or enables exits for a room, again this is normally called by the room manager

#### Map
You set the room data dictionary with your room exits. Most of the functionality is stored in the map base class. In this map, i.e. your map it needs to override some functions. Refer to the existing map file. 

See details above for customing the map data.

#### Room Manager
This does all the work. 

In order to enable/disable debug navigation (i.e. WASD keys as shown above) set the variable debug_mode appropriately.

Methods:
- setup_rooms : this must be called at the start of your game as shown in the sample game.tscn. It requires references to the camera and map scenes, as well as the size of the game room (same as screen size without a hud) and the offset in pixels for the camera navigation (how many pixels off screen the player must go before navigation changes)
- manage_direction: The main navigation function. Tell the room manager to change rooms in a particular direction and with a particular room transition, but only if valid according to the ROOM_DATA array, and raise any signal for play reposition
- change_room: tell the room manager to change room to a specific room number with a particular room transition and ignores the ROOM_DATA allowed directions, i.e. camera will move if the room is a valid room. This will not send the reposition_player signal so you will have to do this yourself, e.g. use one of the get_room_* functions to find the global position of the new room. 

The Room sends the signal 'room_change' when the player hits the RoomNavigation node. You then connect to this signal. In game.tscn you can see this is done via _on_Camera2D_room_change(). In here you normally call manage_direction.

The remaining functions are helper functions and for you to read at your leisure.

All transitions are done here. In your game you may wish to expand on these or even move them to new nodes/code.

## Finishing off
That is all. Why not update the map ROOM_DATA for the complete map and create your own player node to recreate metroid....

In summary:
1. Use the template as your game starter, or if you have a game already just add instances of the other nodes required: room manager, map, camera)

2. Delete GameWithHud.tscn if you are not using a HUD

3. Expand room manager for your own transitions

The transitions you see are just starter transitions and placed directly inside the Room Manager for simplicity. You may want to create your own transition node/class instead of using this hard-coded approach.


