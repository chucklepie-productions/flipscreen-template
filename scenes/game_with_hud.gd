extends Node2D
#Godot 2D Flipscreen Template V1.3.0
#for getting started with the template
#Visit https://gitlab.com/chucklepie-productions/flipscreen-template
#
#For the tutorial in how the template was made
#https://gitlab.com/chucklepie-productions/tutorials/flipscreen-camera
#
#For youtube 
#https://www.youtube.com/playlist?list=PLtc9v8wsy_BY8l7ViNJamL6ao1SKABM5T
#

export(bool) var use_override_settings:=true
export(Vector2) var viewport_size_override=Vector2(-1,-1)
export(Vector2) var viewport_start_location=Vector2(-1,-1)
onready var container=$ViewportContainer
onready var viewport=$ViewportContainer/Viewport
onready var game=$ViewportContainer/Viewport/Game

func _ready() -> void:
	#we do not know the size of the viewport
	#as hud may be any size
	#either manually set them or set the size in the export
	if use_override_settings && (
		viewport_size_override.x<=0 ||viewport_size_override.y<=0):
			print("ERROR. Override set but values are negative. Game may not size or position correctly")
			#just pass game whatever is set
			#we assume negative is ok for start location
			game.configure_game(viewport.size)
			return
			
	if use_override_settings:
		#using overrides. This is usually the easiest way
		#update the position and size
		viewport.size=viewport_size_override
		container.rect_size=viewport_size_override
		container.rect_position=viewport_start_location
		game.configure_game(viewport.size)
		return
		
	#we are here meaning does not want to use overrides
	#ie the settings in viewport and container are to be kept
	game.configure_game(viewport.size)
	
